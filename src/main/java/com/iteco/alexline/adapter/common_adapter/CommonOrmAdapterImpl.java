package com.iteco.alexline.adapter.common_adapter;

import com.iteco.alexline.adapter.structural.home.DbUserEntity;
import com.iteco.alexline.adapter.structural.home.DbUserInfoEntity;
import com.iteco.alexline.adapter.structural.home.orm.first.IFirstOrm;
import com.iteco.alexline.adapter.structural.home.orm.second.ISecondOrm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CommonOrmAdapterImpl implements ICommonOrm {

    private IFirstOrm<DbUserEntity> firstOrm;

    private ISecondOrm secondOrm;

    @Override
    public void createEntity(DbUserEntity entity) {
        firstOrm.create(entity);
    }

    @Override
    public void updateEntity(DbUserEntity entity) {
        firstOrm.update(entity);
    }

    @Override
    public void deleteEntity(DbUserEntity entity) {
        firstOrm.delete(entity);
    }

    @Override
    public void createEntity(DbUserInfoEntity entity) {
        secondOrm.getContext().getUserInfos().add(entity);
    }

    @Override
    public void updateEntity(DbUserInfoEntity entity) {
        secondOrm.getContext().getUserInfos().add(entity);
    }

    @Override
    public void deleteEntity(DbUserInfoEntity entity) {
        secondOrm.getContext().getUserInfos().removeIf(info -> info.getId().equals(entity.getId()));
    }

    @Override
    public DbUserEntity getUserById(int id) {
        return firstOrm.read(id);
    }

    @Override
    public Set<DbUserEntity> getAllUsers() {
        return secondOrm.getContext().getUsers();
    }

    @Override
    public Set<DbUserInfoEntity> getAllInfos() {
        return secondOrm.getContext().getUserInfos();
    }

}