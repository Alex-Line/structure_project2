package com.iteco.alexline.adapter.common_adapter;

import com.iteco.alexline.adapter.structural.home.DbUserEntity;
import com.iteco.alexline.adapter.structural.home.DbUserInfoEntity;

import java.util.Set;

public interface ICommonOrm {

    void createEntity(DbUserEntity entity);

    void updateEntity(DbUserEntity entity);

    void deleteEntity(DbUserEntity entity);

    void createEntity(DbUserInfoEntity entity);

    void updateEntity(DbUserInfoEntity entity);

    void deleteEntity(DbUserInfoEntity entity);

    DbUserEntity getUserById(int id);

    Set<DbUserEntity> getAllUsers();

    Set<DbUserInfoEntity> getAllInfos();

}