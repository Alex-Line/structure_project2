package com.iteco.alexline.adapter.structural.home;

/**
 * IDbEntity.
 *
 * @author Ilya_Sukhachev
 */
public interface IDbEntity {

    Long getId();

    void setId(Long id);
}
