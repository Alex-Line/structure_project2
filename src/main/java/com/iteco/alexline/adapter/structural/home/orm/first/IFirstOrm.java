package com.iteco.alexline.adapter.structural.home.orm.first;

import com.iteco.alexline.adapter.structural.home.IDbEntity;

/**
 * IFirstOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface IFirstOrm<T extends IDbEntity> {

    void create(T entity);

    T read(int id);

    void update(T entity);

    void delete(T entity);
}
