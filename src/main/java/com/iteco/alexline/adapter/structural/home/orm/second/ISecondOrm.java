package com.iteco.alexline.adapter.structural.home.orm.second;

/**
 * ISecondOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrm {

    ISecondOrmContext getContext();
}
