package com.iteco.alexline.adapter.structural.home.orm.second;

import com.iteco.alexline.adapter.structural.home.DbUserEntity;
import com.iteco.alexline.adapter.structural.home.DbUserInfoEntity;

import java.util.Set;

/**
 * ISecondOrmContext.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrmContext {

    Set<DbUserEntity> getUsers();

    Set<DbUserInfoEntity> getUserInfos();

}
